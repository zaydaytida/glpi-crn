# GLPI-CRN

Instalasi IT Asset Management menggunakan GLPI



<br><b>**Menggunakan Docker-Compose**

ketik command 

`sudo docker-compose up -d`
</br></b>


<br><b>**Backup Database**

ketik command 

`sudo docker exec mysql /usr/bin/mysqldump -u glpi --password="YOURMYSQLPASSWORD" glpidb > "YOURFILENAME"`

*hapus tanda petik (")
</br></b>


<br><b>**Restore Database**

ketik command

`cat "YOURFILENAME" | docker exec -i "YOURCONTAINERNAME" /usr/bin/mysql -u root --password=root "DATABASENAME"`

*hapus tanda petik (")
</br></b>

<br>
credit: https://hub.docker.com/r/diouxx/glpi/
</br>
